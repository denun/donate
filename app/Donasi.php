<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    protected $table ='donasi';
    protected $fillable = ['id','kode_donasi','pemilik_id','bencana_id','nama_donasi','jenis_donasi','jumlah_donasi','lokasi_id','deskripsi_donasi','status'];

    public function get_dataUser()
   {
   		return $this->belongsTo(User::class,'pemilik_id');
   }
     public function get_dataBencana()
   {
      return $this->belongsTo(Bencana::class,'bencana_id');
   }


   public function scopeSearch($query, $search){
        return $query->where('kode_donasi','like','%' .$search. '%')->orWhere('nama_donasi','like','%' .$search. '%');
    }
    public function get_lokasi()
   {
      return $this->belongsTo(districts::class, 'lokasi_id');
   }
   public function get_tujuanDonasi()
   {
      return $this->belongsTo(regencies::class, 'lokasi_id');
   }
    public function get_jenisDonasi()
   {
      return $this->belongsTo(Categori::class, 'jenis_donasi');
   }

}
