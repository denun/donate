<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Admin;
use App\Petugas;
use App\User;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function kelolaPetugas(Request $request)
    {
        $search = $request->input('search');
        $Petugas = Petugas::search($search)->paginate(20);
        return view('Admin.kelolaPetugas',compact('Petugas','search'));
      
    }


    public function edit($id)
    {
        $petugas = Petugas::find($id);
        return view('kelolaPetugas',compact('petugas'));
    }

    public function update(Request $request)
    {
        if (!$request->password){
            $id = $request->petugasId;
            $petugas= DB::table('petugas')->where('id',$id);
            $petugas->update([
            'name' => request('nama'),
            'alamat' => request('alamat'),
            'email' => request('email'),
            'hp' => request('hp'),
            ]);
        }
        else {
            $id = $request->petugasId;
            $petugas= DB::table('petugas')->where('id',$id);
            $petugas->update([
            'name' => request('nama'),
            'alamat' => request('alamat'),
            'email' => request('email'),
            'hp' => request('hp'),
            'password' => Hash::make($request->password)
            ]);
        }
        

        return redirect('/kelolaPetugas');
    }

    public function store(Request $request)
    {
        $petugas = new Petugas();
        $petugas->name = $request->nama;
        $petugas->alamat = $request->alamat;
        $petugas->email = $request->email;
        $petugas->hp = $request->hp;
        $petugas->password = Hash::make($request->password);
        $petugas->save();

        return redirect('/kelolaPetugas');


    }

    public function destroy(Request $request)
    {

            $petugas = Petugas::findOrFail($request->petugasIdDelete);
            $petugas->delete();
            return redirect('/kelolaPetugas');
            /*
            $id = $request->petugasIdDelete;
            $petugas = DB::table('petugas')->where('id',$id)->delete();
            return redirect('/kelolaPetugas');*/

    }


    
}
