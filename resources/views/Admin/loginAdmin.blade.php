<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/login2.css">

    <title>DONATE</title>
  </head>
     <body>
      <!-- navbar -->
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="/">DONATE</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin">Dashboard</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- akhir navbar -->
      <div class="jumbotron jumbotron-fluid">
        <div class="row">

          <div class="col-sm-4 offset-sm-4 form ">
                <h2 class="text-center">Admin</h2>
                <div class="col-sm-10 offset-sm-1">
                  <form class="form-signin" method="POST" action="{{ route('admin.login.submit') }}">
                  {{ csrf_field() }}

                  <div class="form-group">
                    <input type="email" id="inputEmail"  name="email" class="form-control" placeholder="Masukkan Email Anda" required autofocus>
                  </div>
                  <div class="form-group">
                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Masukkan Password Anda" required>
                  </div>
                  <div class="row">
                    <div class="col-sm-10 offset-sm-1">
                      <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
                    </div>
                  </div>
                   <br>
                  @if (session('alert'))
                  <div class="alert alert-danger">
                      {{ session('alert') }}
                  </div>
              @endif
                </form>
                </div>

                </div>
              </div>

        <div class="row">
          <div class="col-sm-4 offset-sm-4 footer">
            <p class="text-center">&copy; Copyright 2018 | Built with by<span> De Nun </span>.</p>
          </div>
        </div>
      </div>
    </body>

     <!-- footer -->

    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
