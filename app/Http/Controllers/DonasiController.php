<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Categori;
use App\Bencana;
use App\Donasi;
use App\User;

class DonasiController extends Controller
{
    public function donasiSekarang()
    {
        $id = Auth::user()->id;
    	$categoris = Categori::all();
        $donatur = User::find($id);
    	return view('User.formdonasi',compact('categoris','donatur'));
    }
    public function donasibencana($id)
    {
      
        $categoris = Categori::all();
          $id_donatur = Auth::user()->id;
        $donatur = User::find($id_donatur);
        $bencana = Bencana::find($id);
        return view('User.formdonasiBencana',compact('bencana','categoris','donatur'));
    }
    public function storeDonasi(Request $request)
    {
        $donasi = new Donasi;
        $donasi->kode_donasi = date("dmY").rand(1000,9999);
        $donasi->pemilik_id = $request->pemilik_id;
        $donasi->bencana_id = $request->bencana_id;
        $donasi->nama_donasi = $request->nama_donasi;
        $donasi->jenis_donasi = $request->jenis_donasi;
        $donasi->jumlah_donasi = $request->jumlah_donasi;
        $donasi->lokasi_id = $request->lokasi_id;
        $donasi->deskripsi_donasi = $request->deskripsi_donasi;
        $donasi->status = $request->status;
        $donasi->latitude = $request->lat;
        $donasi->longitude = $request->lng;
        $donasi->save();

        session()->flash('notif','Terima Kasih Telah berdonasi');
        return redirect('/donatur/donasi');


    }
}
